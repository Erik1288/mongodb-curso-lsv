import sys
sys.path.append('..')
from databases.client import MongoLsv

mongo_lsv = MongoLsv()


'''print(
    mongo_lsv.create_new_record_in_collection(
        db_name="Heroes_DB",
        collection="Poder",
        record={"name_poder": "vista lazer"},
    )
)
'''
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="Heroes_DB",
        collection="SuperHeroes",
        record={"poder_id": "62b0f01c03ea232b5427509e", "name_heore": "superboy"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="Heroes_DB", collection="SuperHeroes"
    )
)