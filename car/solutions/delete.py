import sys
sys.path.append('..')

from bson.objectid import ObjectId

from databases.client import MongoLsv

mongo_lsv = MongoLsv()

print(
    mongo_lsv.delete_record_in_collection(
        db_name="Heroes_DB",
        collection="SuperHeroes",
        record_id="62b12060c0b46fc84ab8389a",
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="Heroes_DB", collection="SuperHeroes"
    )
)
