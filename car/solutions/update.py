import sys
sys.path.append('..')

from databases.client import MongoLsv

from bson.objectid import ObjectId

mongo_lsv = MongoLsv()

print(
    mongo_lsv.update_record_in_collection(
        db_name="Heroes_DB",
        collection="SuperHeroes",
        record_query={"_id": ObjectId("62b12060c0b46fc84ab8389a")},
        record_new_value={"name_heore": "clark kent"},
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="Heroes_DB", collection="SuperHeroes"
    )
)
