from dataclasses import asdict, dataclass

@dataclass
class Poder:
    uuid: str
    name_poder: str    

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class SuperHeroes:
    uuid: str
    poder_id: Poder
    name_heroe:str

    def to_dict(self) -> dict:
        return asdict(self)
    
    @property
    def get_poder(self):
        return self.poder_id.label
